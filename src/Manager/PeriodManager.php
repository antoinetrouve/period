<?php

namespace App\Manager;

use App\Model\Absence;
use App\Model\Period;
use DateTime;

/**
 * Class PeriodManager
 * @package App\Manager
 */
class PeriodManager
{
    /**
     * Check if a period is included in another period.
     * The verification include first day of the period but exclude the last day of the period
     *
     * @param Absence $absence
     * @return bool true if a range of the absence given in parameter belongs to the period to check.
     * @see createCurrentMonthlyPeriod
     */
    public function isInclusDansPeriod(Absence $absence): bool
    {
        $currentPeriod = $this->createCurrentMonthlyPeriod();

        return match (true) {
            $absence->getEndDate() < $currentPeriod->getStartDate(),
            $absence->getStartDate() >= $currentPeriod->getEndDate() => false,
            default => true
        };
    }

    /**
     * Create a monthly period : first day of the month until the first day of the next month.
     * @return Period The period created
     */
    private function createCurrentMonthlyPeriod(): Period
    {
        $startPeriod = (new DateTime('first day of this month'))
            ->setTime(0, 0);

        $endPeriod = (new DateTime('last day of this month'))
            ->modify('+1 day')
            ->setTime(0, 0);

        return (new Period())
            ->setStartDate($startPeriod)
            ->setEndDate($endPeriod);
    }
}
