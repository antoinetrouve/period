<?php

namespace App\Command;

use App\Manager\PeriodManager;
use App\Model\Absence;
use DateTime;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:check-period-monthly',
    description: 'Check if absence is included in current monthly period',
)]
class CheckPeriodCommand extends Command
{
    protected LoggerInterface $logger;
    protected PeriodManager $periodManager;
    protected const STARTED_DATE_ARG = "startedDate";
    protected const ENDED_DATE_ARG = "endedDate";
    protected const DATE_FORMAT = "d/m/Y";

    public function __construct(LoggerInterface $logger, PeriodManager $periodManager, string $name = null)
    {
        $this->logger = $logger;
        $this->periodManager = $periodManager;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument(self::STARTED_DATE_ARG, InputArgument::REQUIRED, 'started date in format d/m/Y')
            ->addArgument(self::ENDED_DATE_ARG, InputArgument::REQUIRED, 'ended date in format d/m/Y');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $startedDate = $input->getArgument(self::STARTED_DATE_ARG);
        $endedDate = $input->getArgument(self::ENDED_DATE_ARG);

        $startDateTime = DateTime::createFromFormat(self::DATE_FORMAT, $startedDate);
        if ($startDateTime === false) {
            $io->error(sprintf('wrong date format provided: %s', $startedDate));

            return Command::INVALID;
        }

        $endDateTime = DateTime::createFromFormat(self::DATE_FORMAT, $endedDate);
        if ($endDateTime === false) {
            $io->error(sprintf('wrong date format provided: %s', $endedDate));

            return Command::INVALID;
        }

        if ($endDateTime < $startDateTime) {
            $io->error(sprintf('Your end date (%s) has to be greater than your start date', $endedDate));

            return Command::INVALID;
        }

        $isIncluded = $this->periodManager->isInclusDansPeriod(
            (new Absence())
                ->setStartDate($startDateTime->setTime(0, 0))
                ->setEndDate($endDateTime->setTime(0, 0))
        );

        $message = $isIncluded
            ? 'Your absence IS included in current month period'
            : 'Your absence IS NOT included in current month period';

        $io->success($message);

        return Command::SUCCESS;
    }
}
