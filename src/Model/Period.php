<?php

namespace App\Model;

use DateTimeInterface;

class Period
{
    private DateTimeInterface $startDate;

    private DateTimeInterface $endDate;

    /**
     * @return DateTimeInterface
     */
    public function getStartDate(): DateTimeInterface
    {
        return $this->startDate;
    }

    /**
     * @param DateTimeInterface $startDate
     * @return Period
     */
    public function setStartDate(DateTimeInterface $startDate): Period
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getEndDate(): DateTimeInterface
    {
        return $this->endDate;
    }

    /**
     * @param DateTimeInterface $endDate
     * @return Period
     */
    public function setEndDate(DateTimeInterface $endDate): Period
    {
        $this->endDate = $endDate;

        return $this;
    }
}
