# Period app

![symfony](https://img.shields.io/badge/symfony-5.3-green)
![php](https://img.shields.io/badge/php-%3E%3D8.0-blue)
![php-unit](https://img.shields.io/badge/phpunit-%5E9.5-orange)
![code-sniffer](https://img.shields.io/badge/code--sniffer-%5E3.6-green)

## Description

Symfony command allow checking if an absence is included into a defined period (current month).

## Usage

### Installation
``
composer install
``

### Command

#### Name
``
app:check-period-monthly <startedDate> <endedDate>
``

#### Usage

**Documentation**

``
symfony console app:check-period-monthly --help
``

**Sample**

``
symfony console app:check-period-monthly 27/08/2021 07/09/2021
``

#### Test
You can execute test manually :

``
./vendor/phpunit/phpunit/phpunit
``