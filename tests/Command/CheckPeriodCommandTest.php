<?php

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class CheckPeriodCommandTest extends KernelTestCase
{
    private const COMMAND_NAME = "app:check-period-monthly";
    private const STARTED_DATE_ARG = "startedDate";
    private const ENDED_DATE_ARG = "endedDate";
    private const DATE = '26/08/2021';
    private const WRONG_DATE = '26-08-2021';
    private const BEFORE_DATE = '25/08/2021';

    public function testExecuteOutputSuccess()
    {
        $commandTester = $this->givenCommand();

        $exitCode = $commandTester->execute(
            [
                self::STARTED_DATE_ARG => self::DATE,
                self::ENDED_DATE_ARG => self::DATE,
            ]
        );

        $this->assertEquals(Command::SUCCESS, $exitCode);
    }

    public function testExecuteErrorWrongStartedDateFormat()
    {
        $commandTester = $this->givenCommand();

        $exitCode = $commandTester->execute(
            [
                self::STARTED_DATE_ARG => self::WRONG_DATE,
                self::ENDED_DATE_ARG => self::DATE,
            ]
        );

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('wrong date format provided: ' . self::WRONG_DATE, $output);
        $this->assertEquals(Command::INVALID, $exitCode);
    }

    public function testExecuteErrorWrongEndedDateFormat()
    {
        $commandTester = $this->givenCommand();

        $exitCode = $commandTester->execute(
            [
                self::STARTED_DATE_ARG => self::DATE,
                self::ENDED_DATE_ARG => self::WRONG_DATE,
            ]
        );

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('wrong date format provided: ' . self::WRONG_DATE, $output);
        $this->assertEquals(Command::INVALID, $exitCode);
    }

    public function testExecuteErrorWrongEndedDate()
    {
        $commandTester = $this->givenCommand();

        $exitCode = $commandTester->execute(
            [
                self::STARTED_DATE_ARG => self::DATE,
                self::ENDED_DATE_ARG => self::BEFORE_DATE,
            ]
        );

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString(
            'Your end date (' . self::BEFORE_DATE . ') has to be greater than your start date',
            $output
        );
        $this->assertEquals(Command::INVALID, $exitCode);
    }

    /**
     * Provide the command context to execute test.
     * @return CommandTester
     */
    private function givenCommand(): CommandTester
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find(self::COMMAND_NAME);

        return new CommandTester($command);
    }
}
