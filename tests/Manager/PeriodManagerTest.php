<?php

namespace App\Tests\Manager;

use App\Manager\PeriodManager;
use App\Model\Absence;
use DateTime;
use PHPUnit\Framework\TestCase;

class PeriodManagerTest extends TestCase
{
    private PeriodManager $periodManager;
    private DateTime $initDate;
    private const DATE_FORMAT = "d/m/Y";

    protected function setUp(): void
    {
        parent::setUp();
        $this->periodManager = new PeriodManager();
        $this->initDate = $this->givenInitDate();
    }

    public function testStartAfterStartPeriodAndBeforeEndPeriod()
    {
        // Given input
        $startDate = $this->givenDateTimeFormat($this->initDate, '+1 day');
        $endDate = $this->givenDateTimeFormat($this->initDate, '+3 day');
        $absence = (new Absence())->setStartDate($startDate)->setEndDate($endDate);

        $isIncluded = $this->periodManager->isInclusDansPeriod($absence);

        $this->assertIsBool($isIncluded);
        $this->assertEquals(true, $isIncluded);
    }

    public function testStartBeforeStartPeriodAndBeforeEndPeriod()
    {
        // Given input
        $startDate = $this->givenDateTimeFormat($this->initDate, '-5 day');
        $endDate = $this->givenDateTimeFormat($this->initDate, '+3 day');
        $absence = (new Absence())->setStartDate($startDate)->setEndDate($endDate);

        $isIncluded = $this->periodManager->isInclusDansPeriod($absence);

        $this->assertIsBool($isIncluded);
        $this->assertEquals(true, $isIncluded);
    }

    public function testStartBeforeStartPeriodAndEndAfterEndPeriod()
    {
        // Given input
        $startDate = $this->givenDateTimeFormat($this->initDate, '-5 day');
        $endDate = $this->givenDateTimeFormat($this->initDate, '+60 day');
        $absence = (new Absence())->setStartDate($startDate)->setEndDate($endDate);

        $isIncluded = $this->periodManager->isInclusDansPeriod($absence);

        $this->assertIsBool($isIncluded);
        $this->assertEquals(true, $isIncluded);
    }

    public function testEndBeforeStartPeriod()
    {
        // Given input
        $startDate = $this->givenDateTimeFormat($this->initDate, '-20 day');
        $endDate = $this->givenDateTimeFormat($this->initDate, '-10 day');
        $absence = (new Absence())->setStartDate($startDate)->setEndDate($endDate);

        $isIncluded = $this->periodManager->isInclusDansPeriod($absence);

        $this->assertIsBool($isIncluded);
        $this->assertEquals(false, $isIncluded);
    }

    public function testStartAfterEndPeriod()
    {
        // Given input
        $startDate = $this->givenDateTimeFormat($this->initDate, '+60 day');
        $endDate = $this->givenDateTimeFormat($this->initDate, '+70 day');
        $absence = (new Absence())->setStartDate($startDate)->setEndDate($endDate);

        $isIncluded = $this->periodManager->isInclusDansPeriod($absence);

        $this->assertIsBool($isIncluded);
        $this->assertEquals(false, $isIncluded);
    }

    /**
     * Given an initial date for test context.
     * @return DateTime
     */
    private function givenInitDate(): DateTime
    {
        return new DateTime('first day of this month');
    }

    /**
     * Transform a given string to a date time object formatted in "d/m/Y".
     * @param DateTime $dateTime The string date to format and transform in DateTime object
     * @param string $modifier The modifier to update the date time given in parameter.
     * @return DateTime|false
     */
    private function givenDateTimeFormat(DateTime $dateTime, string $modifier): DateTime|bool
    {
        $date = (clone $dateTime)->modify($modifier)->format(self::DATE_FORMAT);
        return DateTime::createFromFormat(self::DATE_FORMAT, $date)->setTime(0, 0);
    }
}
